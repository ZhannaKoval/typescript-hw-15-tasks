import {IsTypeEqual, ArrayElement, typeAssert} from '../type-assertions/index';
import {nameDecorators} from './index';

typeAssert<
    IsTypeEqual<
        ArrayElement<typeof nameDecorators>,
        (input: string) => string
    >
>();
