declare module 'str-utils' {
    export function strReverse(x: string): string;
    export function strToLower(x: string): string;
    export function strToUpper(x: string): string;
    export function strRandomize(x: string): string;
    export function strInvertCase(x: string): string;
}
