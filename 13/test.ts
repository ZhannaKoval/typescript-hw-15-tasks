import {IsTypeEqual, typeAssert} from '../type-assertions/index';
import { dateWizardFunc, DateDetails} from './index';

typeAssert<
    IsTypeEqual<
    DateDetails,
        {
            year: number;
            month: number;
            date: number;
            hours: number;
            minutes: number;
            seconds: number;
        }
    >
>();

typeAssert<
    IsTypeEqual<
        typeof dateWizardFunc.pad,
        (level: number) => string
    >
>();

