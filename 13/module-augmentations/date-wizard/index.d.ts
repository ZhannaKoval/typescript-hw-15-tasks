// This enables module augmentation mode.

   export interface DateDetails {
        year: number;
        month: number;
        date: number;
        hours: number;
        minutes: number;
        seconds: number;
    }
    const pad: (s: number) => string;
    const dateDetails: (date: Date) => DateDetails;
    const dateWizard: (date: Date, format: string) => string;


const dateWizardFunc = {
    pad,
    dateDetails,
    dateWizard
}

export default dateWizardFunc;



